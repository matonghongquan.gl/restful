﻿using LAB6_RESTFUL_API.Models;
using LAB6_RESTFUL_API.Respositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LAB6_RESTFUL_API.Controllers
{
    
    [ApiController]
    [Route("api/products")]
    public class ProductApiController : ControllerBase
    {
        private readonly IProductRespository _productRespository;

        public ProductApiController(IProductRespository productRespository)
        {
            _productRespository = productRespository;
        }
        [HttpGet]
        public async Task<IActionResult> GetProduct()
        {
            try
            {
                var prodcut = await _productRespository.GetProductsAsync();
                return Ok(prodcut);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }

        }

        [HttpGet("id")]
        public async Task<IActionResult> GetProdutByIdAync(int id)
        {
            try
            {
                var product = _productRespository.GetProductByIdAsync(id);
                if (product == null)
                    return NotFound();

                return Ok(product);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddProduct([FromBody] Product product)
        {
            try
            {
                await _productRespository.AddProductAsync(product);
                return CreatedAtAction(nameof(GetProdutByIdAync), new { id = product.Id });
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProduct(int id, [FromBody] Product product)
        {
            try
            {
                if (product != null)
                    return BadRequest();
                await _productRespository.UpdateProductAsync(product);
                return NoContent();

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete("id")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            try
            {
                await _productRespository.DeleteProductAsync(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
