﻿using Microsoft.EntityFrameworkCore;
using LAB6_RESTFUL_API.Models;
namespace LAB6_RESTFUL_API.Data
{

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
    }
}
