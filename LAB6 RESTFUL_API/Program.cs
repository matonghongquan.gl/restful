using LAB6_RESTFUL_API.Data;
using LAB6_RESTFUL_API.Respositories;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<ApplicationDbContext>(options => 
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConection"))); 


builder.Services.AddControllers();
builder.Services.AddScoped<IProductRespository, ProductRepository>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors(options =>
 {
        options.AddPolicy(name: "MyApi", policy =>
        {
            policy.WithOrigins("http://172.0.0.1.5500", "http://localhost:5500")
            .AllowAnyHeader()
            .AllowAnyMethod();
    });

 });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseCors("Myapi");
app.UseAuthorization();

app.MapControllers();

app.Run();
