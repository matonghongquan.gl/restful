﻿using LAB6_RESTFUL_API.Models;

namespace LAB6_RESTFUL_API.Respositories
{
    public interface IProductRespository
    {
        Task<IEnumerable<Product>> GetProductsAsync();
        Task<Product> GetProductByIdAsync(int id);

        Task AddProductAsync(Product product);
        Task UpdateProductAsync(Product product);
        Task DeleteProductAsync(int id);
    }
}
